from django.contrib import auth, messages
from django.contrib.auth.models import User
from django.shortcuts import redirect, render

from multiverse.models import Universe
from characters.models import Character
# from contacts.models import Contact


def index(request):
	return redirect('login')


def register(request):

	""" Allows a user to signup """
	if request.method == 'POST':
		# Register user

		first_name = request.POST.get('first_name')
		last_name = request.POST.get('last_name')
		username = request.POST.get('username')
		email = request.POST.get('email')
		password = request.POST.get('password')
		password2 = request.POST.get('password2')

		# Check username
		if User.objects.filter(username=username).exists():
			messages.error(request, "User already exists.")
			return redirect('register')

		if User.objects.filter(email=email).exists():
			messages.error(request, "Email is already registered with a user.")
			return redirect('register')

		if password != password2:
			messages.error(request, "Passwords don't match")
			return redirect('register')

		# All looks well.
		user = User.objects.create_user(username=username, password=password, email=email, first_name=first_name, last_name=last_name)
		user.save()
		messages.success(request, 'You are now registered and can login!')
		return redirect('login')

	else:
		return render(request, 'accounts/register.html')


def login(request):

	""" Allows a registered user to login """

	if request.user.is_authenticated:
		return redirect('characters')

	if request.method == 'POST':
		username = request.POST.get('username')
		password = request.POST.get('password')

		user = auth.authenticate(username=username, password=password)
		if user:
			auth.login(request, user)
			messages.success(request, 'You are now logged in!')
			return redirect('characters')

		else:
			messages.error(request, 'Invalid credentials.')
			return redirect('login')

	return render(request, 'accounts/login.html')


def logout(request):
	if request.method == 'POST':
		auth.logout(request)
		messages.success(request, 'You are now logged out.')

	return redirect('characters')


def dashboard(request):

	if not request.user.is_authenticated:
		return render(request, 'accounts/login.html')


	universe = Universe.objects.filter(creator=request.user)[:3]
	characters = Character.objects.filter(creator=request.user).order_by('-name')

	context = {
		'universe': universe,
		'characters': characters,
		'page_title': "Your multiverse dashboard of characters",
	}

	return render(request, 'accounts/dashboard.html', context)
