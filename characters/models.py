from django.db import models
from datetime import datetime
from django.contrib.auth.models import User
from multiverse.models import Universe


class Character(models.Model):
	creator = models.ForeignKey(User, on_delete=models.DO_NOTHING)
	name = models.CharField(max_length=200)
	kind = models.CharField(max_length=100)
	description = models.TextField(blank=True)
	photo_main = models.ImageField(upload_to='photos/%Y/%m/%d', blank=True)
	# thumbnail = models.ImageField(upload_to='thumbnails', editable=False, blank=True)
	universes = models.ManyToManyField(Universe, blank=True)

	creation_date = models.DateTimeField(default=datetime.now, blank=True)

	def __str__(self):
		return self.name

	def save(self, *args, **kwargs):
		""" Make and save the thumbnail for the photo here. """

		super(Character, self).save(*args, **kwargs)

	class Meta:
		get_latest_by = 'creation_date'
