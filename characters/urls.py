from django.urls import path
from . import views

urlpatterns = [
	path('', views.index, name='characters'),
	path('new', views.create, name='create-character'),
	path('<int:character_id>/edit', views.edit, name='edit-character'),
	path('<int:character_id>', views.view, name='character'),
	path('<int:character_id>/add', views.add, name='add-to-universe'),
	]