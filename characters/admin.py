from django.contrib import admin
from .models import Character


class CharacterAdmin(admin.ModelAdmin):
	list_display = ('id',
					'name',
					'kind',
					'creation_date')

	list_display_links = (
		'id',
		'name'
	)

	search_fields = (
		'name',
	)


admin.site.register(Character, CharacterAdmin)