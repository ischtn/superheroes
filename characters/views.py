from django.core.files.storage import FileSystemStorage
from django.db.models import Q
from django.shortcuts import render, redirect, get_object_or_404

from .models import Character
from django.contrib import messages
# from sorl.thumbnail import get_thumbnail
from multiverse.models import Universe
import logging


log = logging.getLogger(__name__)


def index(request):
	log.critical("BWAAAH!")

	if not request.user.is_authenticated:
		return render(request, 'accounts/login.html')

	characters = Character.objects.order_by('-kind')

	context = {
		'characters': characters,
		'page_title': 'All characters',
	}

	return render(request, 'characters/index.html', context=context)


def view(request, character_id):
	character = get_object_or_404(Character, pk=character_id)

	# im = get_thumbnail(character.photo_main, '100x100', crop='center', quality=70)

	universes = character.universes.all()

	allowed_lists = Universe.objects.filter(Q(is_private=False) | Q(creator=request.user)).exclude(id__in=universes)

	context = {
		'character': character,
		'universes': universes,
		'allowed_lists': allowed_lists,
	}

	return render(request, 'characters/character.html', context)


def create(request):
	if not request.user.is_authenticated:
		# User must be logged in.
		return render(request, 'accounts/login.html')

	if request.method == 'GET':
		return render(request, 'characters/create.html')

	elif request.method == 'POST':

		# Check if character can be added.
		name = request.POST.get('name')
		kind = request.POST.get('kind')
		description = request.POST.get('description')


		myfile = request.FILES.get('photo_main')
		log.info(f"My file: {myfile}")

		# Check if this character does not already exist.
		if Character.objects.filter(creator=request.user, name=name, kind=kind).exists():
			messages.error(request, f"You already have a {kind} called {name}")
			return redirect('add-character')

		# All looks well.
		filename = None
		if myfile:
			fs = FileSystemStorage()
			filename = fs.save(myfile.name, myfile)

		character = Character.objects.create(name=name, kind=kind, description=description, photo_main=filename, creator=request.user)
		character.save()
		messages.success(request, f"{name} the {kind} has been added to your Universe!")

		return redirect('characters')


def edit(request, character_id):

	character = get_object_or_404(Character, pk=character_id)

	context = {
		'character': character,
	}

	if not request.user.is_authenticated:
		return render(request, 'accounts/login.html')

	if request.user.id is not character.creator.id:
		messages.error(request, f"{character.name} the {character.kind} is not your character. You cannot edit characters from other creators.")
		return render(request, 'characters/character.html', context=context)

	if request.method == 'GET':
		return render(request, 'characters/edit.html', context=context)

	elif request.method == 'POST':
		name = request.POST.get('name')
		kind = request.POST.get('kind')
		description = request.POST.get('description')
		photo_main = request.POST.get('photo_main')

		if character.photo_main:
			photo_main = character.photo_main.url

		# All looks well.
		myfile = request.FILES['photo_main']
		if myfile:
			fs = FileSystemStorage()
			filename = fs.save(myfile.name, myfile)
			photo_main = filename

		Character.objects.filter(id=character_id).update(name=name, kind=kind, photo_main=photo_main, description=description)
		messages.success(request, f"{name} the {kind} has been updated in your universe.")

		return redirect('/characters/' + str(character_id))


def add(request, character_id):

	""" Add a character to a universe """

	if not request.user.is_authenticated:
		return render(request, 'accounts/login.html')

	if request.method == 'POST':

		universe_id = request.POST.get('list')

		universe = get_object_or_404(Universe, pk=universe_id)
		character = get_object_or_404(Character, pk=character_id)

		if not request.user.id == universe.creator_id:
			messages.error(request, 'You can only add character to your own universe.')
			return redirect('/characters/' + str(character_id))

		character.universe.add(universe)
		character.save()

		messages.success(request, f'{character.name} was added to {universe.name}')

	return redirect('/characters/' + str(character_id))
