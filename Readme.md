# Superhero Multiverse registry

Themed as a registry of different characters (or heroes, but although
comics don't tend to advertise this very often, that is quite a subjective definition...)
from different universes.

Create a 'universe' to register your superheroes (or characters).
Assign characters to multiple universes in the multiverse.
'Share' your characters or keep them private,
Only allow the creator to edit characters, but allow others to add them in their own universes.

Quite a meaningless project.. intended to just fiddle around a bit in a Django project every once in a while.
Also, like every heroes universe, highly unstable and always WIP.
