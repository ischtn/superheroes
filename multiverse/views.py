from django.db.models import Q
from django.shortcuts import render, get_object_or_404, redirect

from .models import Universe
from characters.models import Character
from django.contrib import messages


def index(request):

	if not request.user.is_authenticated:
		return render(request, 'accounts/login.html')

	multiverse = Universe.objects.order_by('-name')

	context = {
		'multiverse': multiverse,
	}

	return render(request, 'multiverse/index.html', context=context)


def view(request, universe_id):

	if not request.user.is_authenticated:
		return render(request, 'accounts/login.html')

	universe = get_object_or_404(Universe, pk=universe_id)

	characters = universe.character_set.all()

	allowed_characters = Character.objects.filter(creator=request.user).exclude(id__in=characters)

	print(allowed_characters)

	context = {
		'universe': universe,
		'characters': characters,
		'page_title': f'Character in {universe.name}',
		'allowed_characters': allowed_characters,
	}

	return render(request, 'multiverse/view.html', context=context)


def create(request):
	if not request.user.is_authenticated:
		return render(request, 'accounts/login.html')

	if request.method == 'GET':
		return render(request, 'multiverse/create.html')

	elif request.method == 'POST':

		# Check if character can be added.
		name = request.POST.get('name')
		description = request.POST.get('description')
		is_private = True if request.POST.get('is_private') is 'on' else False

		if Universe.objects.filter(creator=request.user, name=name).exists():
			messages.error(request, f"You already have a universe called {name}.")
			return redirect('add-character')

		universe = Universe.objects.create(name=name,  description=description, creator=request.user, is_private=is_private)
		universe.save()

		messages.success(request, f"Your universe {name} was created. Start adding characters now!!")

		return redirect('/lists/' + str(universe.id))


def add(request, universe_id):
	""" Add a character to a universe """

	if not request.user.is_authenticated:
		return render(request, 'accounts/login.html')

	if request.method == 'POST':
		character_id = request.POST.get('character_id')

		universe = get_object_or_404(Universe, pk=universe_id)
		character = get_object_or_404(Character, pk=character_id)

		character.multiverse.add(universe)
		character.save()

		messages.success(request, f'{character.name} was added to {universe.name}')

	return redirect('list/' + universe_id)
