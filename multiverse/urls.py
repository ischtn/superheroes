from django.urls import path
from . import views

urlpatterns = [
	path('', views.index, name='index'),
	path('create', views.create, name='create-universe'),
	path('<int:universe_id>', views.view, name='universe'),
	path('<int:universe_id>/add', views.add, name='add-character'),
	]