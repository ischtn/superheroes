from django.db import models
from datetime import datetime
from django.contrib.auth.models import User


class Universe(models.Model):

	creator = models.ForeignKey(User, on_delete=models.DO_NOTHING)
	name = models.CharField(max_length=100)
	description = models.TextField(blank=True)

	is_private = models.BooleanField(default=False)
	creation_date = models.DateTimeField(default=datetime.now, blank=True)


	def __str__(self):
		return self.name
