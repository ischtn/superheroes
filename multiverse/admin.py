from django.contrib import admin
from .models import Universe


class SuperheroesAdmin(admin.ModelAdmin):
	list_display = ('id',
					'name',
					'creation_date')

	list_display_links = (
		'id',
		'name'
	)

	search_fields = (
		'name',
	)


admin.site.register(Universe, SuperheroesAdmin)