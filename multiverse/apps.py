from django.apps import AppConfig


class MultiverseConfig(AppConfig):
    name = 'multiverse'
